<?php

/******************************************************************************/
/* backend save to db *********************************************************/
/* author: Michail Maridakis **************************************************/
/* email: michael.marid@gmail.com *********************************************/
/******************************************************************************/

/* connect to db **************************************************************/
include_once "../models/data_class.php";
$myPDO = new SQLITE_PDO("../_data/form.db");

//connection testing
//if ($pdo != null) echo 'Connected to the SQLite database successfully!'; else echo 'Whoops, could not connect to the SQLite database!';

//SOS: UNCOMMENT ONLY ON NEW INSTALLATION
$myPDO->createDatabase();

// λήψη μεταβλητών input ως json
$json = file_get_contents('php://input');
// μετατροπή σε stdclass
$params = json_decode($json);

// αποθήκευση input σε μεταβλητές
$curDate = "CURRENT_TIMESTAMP";
$firstName = $params->firstName;
$lastName = $params->lastName;
$fatherName  = $params->fatherName;
$citizenShip = $params->citizenShip;
$genderName = $params->genderName;
$birthPlace = $params->birthPlace;
$dateArray = explode("-", $params->birthDate); // fix date format YYYY-MM-DD
$x = $dateArray[0];
$dateArray[0] = $dateArray[2];
$dateArray[2] = $x;
$birthDate = implode("-", $dateArray);
$paperId = $params->paperId;
$dateArray = explode("-", $params->idDate);
$x = $dateArray[0];
$dateArray[0] = $dateArray[2];
$dateArray[2] = $x;
$idDate = implode("-", $dateArray);
$vatNum = $params->vatNum;
$vatService = $params->vatService;
$securityNum = $params->secNum;
$streetRoad = $params->address;
$streetNum = $params->addressNum;
$postal = $params->zipCode;
$city = $params->city;
$province = $params->province;
$telephone = $params->phone;
$email = $params->email;
$maritalStatus = $params->maritalStatus;
$children = $params->children;
$labourStatus = $params->labourStatus;
$labourName = $params->labourName;

// εκτέλεση ερωτήματος αποθήκευσης στη ΒΔ
$myPDO->save($firstName, $lastName, $fatherName, $citizenShip, $genderName, $birthPlace, $birthDate, $paperId, $idDate, $vatNum, $vatService, $securityNum, $streetRoad, $streetNum, $postal, $city, $province, $telephone, $email, $maritalStatus, $children, $labourStatus, $labourName);

// επιστροφή json με την απάντηση (για API)
class Result {}
$response = new Result();
$response->result = 'OK';
$response->message = 'Καταχωρήθηκε';
header('Content-Type: application/json');
echo json_encode($response);

?>
