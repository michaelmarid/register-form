<?php

/******************************************************************************/
/* register form **************************************************************/
/* author: Michail Maridakis **************************************************/
/* email: michael.marid@gmail.com *********************************************/
/* guide: Customization in js/libraries ***************************************/
/******************************************************************************/

/* error reporting ************************************************************/
error_reporting(E_ALL);
ini_set("display_errors", 1);

/* mini router ****************************************************************/
$content = include_once "views/form.php";

?>

<!DOCTYPE html>
<html lang="el">
	<head>
		<!-- meta data -->
		<meta charset="utf-8" />
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
		<meta name="author" content="michael.marid" />
		<meta name="robots" content="index, follow" /> 
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta http-equiv="imagetoolbar" content="no" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<meta name="apple-mobile-web-app-capable" content="yes">
	
		<!-- title -->
		<title>Registration Form</title>
		
		<!-- styles -->
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
		<link href="css/bootstrapValidator.min.css" rel="stylesheet" type="text/css" />
		<link href="css/daterangepicker.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		
		<!-- js libraries -->
		<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
		<script type="text/javascript" src="js/libraries/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="js/libraries/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/libraries/bootstrapvalidator.min.js"></script>
		<script type="text/javascript" src="js/libraries/angular.min.js"></script>
		<script type="text/javascript" src="js/libraries/pdfmake.min.js"></script>
		<script type="text/javascript" src="js/libraries/vfs_fonts.js"></script>
		<script type="text/javascript" src="js/libraries/moment.min.js"></script>
		<script type="text/javascript" src="js/libraries/daterangepicker.js"></script>
		
		<!-- js for customization -->
		<script type="text/javascript" src="js/custom/angular_ctrl.js"></script>
		<script type="text/javascript" src="js/custom/validator.js"></script>
		<script type="text/javascript" src="js/custom/createpdf.js"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
  
	</head>

	<body>
	
		<div class="container">

			<?php
				// dynamic content from router
				echo $content; 
			?>

		</div> <!-- container -->

	</body>
</html>
