//http://pdfmake.org/playground.html

// παραγωγή pdf με βάση τη φόρμα εισαγωγής
function exportPDF(){
	
	// αποθήκευση δεδομένων από τον πίνακα επιβεβαίωσης (προβολή 2) - SOS: είναι όλα τα μονά td - π.χ. 1,3,5,7,...
	var data = document.getElementById("toPdf").getElementsByTagName("td");
	
	// εκκίνηση pdfmake
	var docDefinition = {
		
		// footer κάθε σελίδας
		footer: function(pagenumber, pagecount) {
			return {
				alignment: 'center',
				text: 'Φόρμα Εγγραφής στην Εταιρεία ΧΧΧ\n' + pagenumber + ' / ' + pagecount
			};
		},
		// περιεχόμενο
		content: [
		
			{
			// image converter to base64 => https://www.base64-image.de/
			image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAVsAAACtCAYAAAAec9xdAAAABGdBTUEAALGeYUxB9wAAACBjSFJNAACHEAAAjBIAAP1NAACBPgAAWesAARIPAAA85gAAGc66ySIyAAABLWlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAACjPY2BgMnB0cXJlEmBgyM0rKQpyd1KIiIxSYD/PwMbAzAAGicnFBY4BAT4gdl5+XioDKmBkYPh2DUQyMFzWBZnFQBrgSi4oKgHSf4DYKCW1OBlopAGQnV1eUgAUZ5wDZIskZYPZG0DsopAgZyD7CJDNlw5hXwGxkyDsJyB2EdATQPYXkPp0MJuJA2wOhC0DYpekVoDsZXDOL6gsykzPKFEwtLS0VHBMyU9KVQiuLC5JzS1W8MxLzi8qyC9KLElNAaqFuA8MBCEKQSGmAdRooclAZQCKBwjrcyA4fBnFziDEECC5tKgMFhdMxoT5CDPmSDAw+C9lYGD5gxAz6WVgWKDDwMA/FSGmZsjAIKDPwLBvDgDCs0/+1ia4KQAAAAlwSFlzAAAQTQAAEE0BZ4wB4AAAAAZiS0dEAP8A/wD/oL2nkwAAAAd0SU1FB+IDBg0QAlmH1QsAABBGSURBVHhe7d29dtrKGsbxl3MJuz67gxRZuQJ8BThNqrTpoIQmXUp3aaCEzm2qNIErMFfg5cLQ7frsW+BoJGHzISSNNF+S/r+1tDZJvB3HHj2886GZ3iEiAACr/pP+FwBgEWELAA4QtgDgAGELAA4YmSD79+9B+goA2uWvf3bpq3oqhy0BC6Br6gSvdtgSsgC6rkroaoUtQQsA73RCt/QEGUELAOd0crGwsi37yUwNIgNAKEzmX+2wJWQBdEHdLMwN27xPTsgC6KKquXhzzJagBYBrefmXl5uZYUvQAsBtVQJX63FdghYAErp5eBW2t1KZoAWAc7dyMStHtSpbAEA1pcKWqhYAspXNx7OwzZsYAwCUd5mnhZUtVS0A5CuTk4zZAoADhC0AOEDYAoADDQnbvSzuetLrZV0T2aQfBQChCjxsNzKJA3Ugs236W1dWcn+3iOIYAMIVbNhuJipk76MoLWE7k5+UtwACFl7Ybibx8MB9qZR9t/pN2gIIV0Bhm47L6qbs0fMrQwkAghVG2O4Xcpc7LlvC9kV4/AJAqLyH7X5xJ73BTOrkbOJZXiltAQTKa9iqSbBBrXL21FZeKG0BBMpT2Cbjs1WHZwGgadyHrYnxWQBoGLdhq4LWyPhstmcGbQEEyl3YqvWzFoMWAELmJGzjFQcM0ALoMOthq4LW3IqDfFuWIwAIlNWwNbu0CwCay1rYqqBl5AAAEpbCdi+vz+lLAICtsO3L9Mc4eTley+FweL92cxkmfwIAnWFvzHa0TMJ1OUp/I9WfyuPcUtyy8xeAQFmdILulP/0had0LAJ3gJWyjsle+kLYAOsRT2EZxS9oC6BBvYSuDj0yUAegMf2Hb/yxfSVsAHeEvbKUvHz6lLwGg5TyGrRpJoLQF0A1ew7ZPaQugI7yGLZNkALrCb9gySQagI/yGLZNkgCHJIaq93vs12aR/hCB4DlsmyYKjji86uWEvL27gS9chd3bV+oYVfO6z6/wQ1eF8J5fbkgSnY23Ne9jCs/i045NGXrAJ8er+5GM7l7xZ4VdwUvTq/uLjda6Kp1CP1/I07ae/CEjH25r3sGVFgh9qc/e4Edc5hPMYJHeLVu+2Fp+hF9/0DTiCfziXXWAlLW0tQWXbKe+VmdFTNLYzGagboWWV7jFkm3O001jWT1MJo6alrV3yH7Ys/3Lg2PAtV2Zx9XEni8aXuRuZNCpklaHMd0vxX9PS1m6hsm25pAvnsvu7ldmgJ3cNTdykmr2Xph2fN14/ie9hWtpaPsK2pY5dYF+Hbm5ng4Z19ZpYzSZ8rzygrZVD2LZNOuMbRGiorl4TAjdegtS8ajbmc+UBbU0LYdsicTeuzoyvDdFNEHI3L67KfJVkdXlceUBb00fYtkLSBQ41M1Q3L7yiI5nIaeKwQUwFrZeVB7S1qgjbpmtIF3h1H9LMsQqMBqyZvcnTEi/aWi2EbaNFodGYLvBWZt8CWJAejzM2dHw25muJF22tLv9h2/8gPENW1UAatbXEdibffJYcqjILbZxRk78lXrS1uqhsG60vnx93cjgcyl+7udeHSLazn1GN5F5TJ8LUsq7Tn5+/JV60tboI24br9zXLnP5Uno43w9rHcfIreXBccaiZc52JsGPA7ebuomK8PgmpkyukDWVoa/UQtl02Wnq5EVxWHCpoiwray6A7Blx/+nT2+4fDThzmb7t0oK0VIWzbarMoPyMb3wgug2Qlvx3cAYVBO17HAVC+a96X6ROhe4W2Vkq7wnb7Irv0ZXcl6yB7DyKftXp9SZC46jqvHmzOFidraPOCNq5mKw+Auv1ehYu2piOAsG3YLGfATjdRGX79HDVpfXHX2cXExvaX/LFyB6igzV9Dq4LWxERT/L3yMhbpH21NH8MIrZBUGO+TQEP5qldqnFMTG9Zvgq38snAHbCYFDyuM12Zn9KNucf0KbSgfB+nL4NHWqiJsmy7rqZ7hV81uXQYHN8H21x+j3bviybCxrC2snVIVWr0C95N8qPvzcoG2Vgth22DxZiAZ6VK1W3dF3QQ2u8kGu3eqW1u06kDGX6w9eTVarqMor2j4UUIvbGlr9RG2jZQ3AVSzW3fJSDf5lq28GJjRVEFbZh3t+IutqFVG8r3i98lYYFlBWzMlgLDtC2c+6lBjZjnjkia6dRf600drS3VWddflRF3bcg8sjMVq1kb6n79W6AqP5UdADy6co62ZRGXbJGU2Ufn0wUKVpJbq1Ogm53l+rTGWFoVB4dhBykVXvf9ZvmoGxXgdwrlhGWhrxhG2TaEaf4lNVOx1lat3k3NVXhuturcFYeCcZi/N9MoIU2hrVhC2TVB6tyq7S4jsdPGe5bVCubFffNPbj9bRjTYou2hcPb0WYtLS1qwhbEOnGn/ZrrL1JURRF+/R9BKdChMXUeX1TStpFb832pmQg5a2Zg1hGzKtxh9xMi45lR8WV+gU28viW5U9acNY2N6OoI10oq2ZRdiGSo2b6TR+h0bfbT/xk2PzU2/44ITvhe3x1o0hBi1tzYkgwrb0OFdnbGQS8okC3ioOjdUHWRzs3r97yf6pqaANaW/ad7Q1V6hsA7SZhDbLfs1HxbFfPNT+vmxn3yweBriR3xlfYLhBS1tzibANzWZS/NhpCCqsKc1WdlZ7L39+mai/LB4GuPl9EVzqcMawTls4Q1tzqmVhG9CMc0WbrNKoLKfrCPvy2cgdUHJWu8ZY7ZXtTAZ3pgN3L4uH05+dClpfhzOWQ1tzi8o2JPuFnN2v2ty+2VR7PPVCyVntWsGQRQVubxJ1/E1RTz4dTyAYy/oQdtDS1twjbAOy//Or5kSF43WEJrp3pR753Mvrc/rSqJXc93pyZ2wQVwWuOjYn0EdwT9DW3CNsA3JrJlvHs9NxlPqbCJV65HP/R4wM196wnQ2kF4XuJJCzqlygrblH2AYjeyZbl+u1pPWW7dnfiUvH6r4Xh258tTp5aWs+ELZt43izjX6dcqPsZt67l5pd3gpW9+/B+3aZHONtgTa2NYtaFrb+NwiubP8qZoYlHR/dPPhYeeLCd7dOXzLGex3CdxbX7lpAW/OCyraFQjm6OZ9Gt67GTebGVmaDrBBu/zhw69qaRYRtKEx2lV2et9T/IFU6d8P59/Lduop/RwjOxoFPLnMrICqgrXlB2LZSVGn9DLmk0j0KZiRfWrT7k3JcARFUCFfStrZmD2EbCtNd5dVDsOOIVSqNNu3+lOc8hC2NBdPWvCBsW8viHgB1DOfyWKXSaNnepuW8jwWHXfG2rK1Z0rqwdbvQ2iAb45IOthTUM5T547TyUzyj5fFx2O4x+uAFbc0LKttgDMTGtr52txSMaCwjGs4fa+4XYPHk1YZIJtzqrvelrflA2Aaj/uOI2VRX1OJi/LIz28a6dCNZHroduFHkxut9q1e5tDUfCNuA2DuxQt2cdm6Ccrtxme7SqcDt7pDCkapyqwYubc09wtahzeQ4y3x9qQkQI9vI3WTjJij3jP14bWO7wXSHrXXHa9yKgUtbc4+wrUOdSJoRnLeuW7vivx2bYmxH+lvUTWBwOdHVyQTX1L/N6hmHo6UcDgfpcuau7isEG23Nvaihvvnff/tXlwu7+fCgvpQuXlEDSb8LCVffi8u/V9/6EOVb5ud+u8br9GPd6WpbqvLzpK2ZVZSfhK3HK7MR7uaHqODI/Hjz1/BQ9T6IKsmMz3dyBdD4j7rRvsZRJGmirRlVlJ+Eracr792+sHGZvobzg859UPj1aX4+f0pUTA26qmQObc2covwkbD1chTeF04rj9Cqojsp8XY0J2gLrcfa/L+SrStrS1owpyk/C1vFV9n5o5PckkO6cbc6rwbJXxe8/bc2MovxkNYJDQ43Z0v70R7MW7o/Xcgh6Ktic0TIuUjIuzw9bPL9W2p+AtuYGYeuICtp4eVdpI1lGJVQTqH9bV4I2n3rY4jyAo6ox/bOQ0dZcIGwd0A/a1GgZ/M1a+d/WEf3p01vwWs+zOkd109asI2wtq9tA1M0aatExXh8IWg3J8IO9x4yHHwfpq2poa3YRtjaN10YaSHhbC45lHVVqjBxUkTxmbCPUPn2grYWMsLXF6CB+QFsLDueyOyyFnK1ntDT98zR1qCFtzZYgwnb3Yuz4uTCoRmL8rdj/1oKqK3d4CnNHpeYxPCk1/mIwlGhrNlDZmqaC1loj8XUTMGxgxeiLoZ/lUObf2/Lm3t62RtgaFTUU6+/G6iZwN64WVxgMG1hi6MSE8Q9L2wrS1kwibI1R78iuGkoyyWJ1qY4ac6aatczEiQk2qtpTtDVTCFsjoga/c/+OnKzhNNzViyclogqDlHWi7okJrs7aoq3VR9ga4Hd3eNXVixrsbl5v5/20umACrEGisHJ71hZtrY4gwvb2s+bNuIJ4Y+5P5Sn9ekpPch8bvbqoZBvG41lbtLVKqGxbqPSbVxca/dvRRXYOIayr6rLHUM7aoq2VR9iitfaLO+m9Hfxm79TX6sodYnhJPQJOdjUPYYsW2svirieD2XnVOF6Htaxov3goPMTwStQdZz+KZiJs0TIbmfQGcpGzcUiFVQ1u5OfVF1nAypOJcIWwRXvsF3LXu8+sFsdmNg4wZjPJ/jpvc/HADGwibNEOaiJsMJPsWnEoNXcfNEqNJb8NJZfi8oEZ2ELYovHOJ8KyfBIDuw+aEb0pXI4l54oX/hO0bUDYotE2k+uJsGvP8lrlcC7TVPWtU9KqoGXooDUIWzRUsuKgXHZt5WWXvvREvSloBa16CICgbRXCFg10Y8VBjtVvXyts1dda9k0hodbR8hBA+xC2aJacFQe5Vg+ycD2UED+9pvO1qg2NONetrQhbNEfuioMiW5kNHD1BFoes5rBBPBEWxiO4sIOwRTPoTi5lUo/s3tmrcOOqWzNkI/GwAeOzrUfYInxGgvZIVbhmN6aJl56pkNWuupMjYBg26AbCFmEzGrSnVJWrQlddutVushIi+X/LLD27FlezrJ/tlN5B7X+W+vfv68ds/vrH85oZdJfqllceow0VT4O1VVF+UtkiWJuf7QpaDs/sNsIWYYqq2gcbowceJEMGHJ7ZdYQtwrR7aX5Vmx4FwwQYFMIWMC1eM8tRMDhH2CJMg4/1TnD14XioIWtmkYGwRZj6H+RT+jJ0ycQXlSzyEbYI1Ei+zwOubY9DBdFFxqIMwhbB6k+fZD1OfxGItyqWoQJoImwRtNEyCjbPiXtcukUVizoIW4RvtEzCzmHongYsS7dgAmGL5jiGbnTtTI/nHlcSELCwhLBFI6nx3NNwPBzWUrbuPa1a3y7GB2AZG9EAgAFsRAMAASBsAcCBwrDNKo0BAO/K5ORZ2DI+CwBmXOYpwwgA4ECpsGUoAQCylc3Hq7C9NZRA4ALAuVu5mJWjWsMIBC4AJHTzMDNs8ybKCFwAXZeXg7fy82ZlS+ACwLUqQaucPa57qUyo5n1yAGiLojwsysLcsFXKVrGELoA2MpWBhWF7VPYvBIAuKVtoll6NQOUKAOd0crF0ZXtEhQug66oUn9phe0ToAuiiqr38ymF7iuAF0FamhlCNhC0AIJ/W47oAgGoIWwBwgLAFAOtE/g+Gm2VsWXMmaAAAAABJRU5ErkJggg==',
			width: 70,
			alignment: 'center'
			},
			{text: 'Φόρμα Εγγραφής για τις υπηρεσίες της εταιρείας ΧΧΧ', bold: true, alignment: 'center', fontSize: 14},
			{text: '\n\nΣτοιχεία Μέλους', alignment: 'center'},
			{
				table: {
					widths: [ '*', 'auto'],
					body: [
						[{text: 'Όνομα', fillColor: '#77caea'}, data[1].innerHTML],
						[{text: 'Επώνυμο', fillColor: '#77caea'}, data[3].innerHTML],
						[{text: 'Πατρώνυμο', fillColor: '#77caea'}, data[5].innerHTML],
						[{text: 'Υπηκοότητα', fillColor: '#77caea'}, data[7].innerHTML],
						[{text: 'Φύλο', fillColor: '#77caea'}, data[9].innerHTML],
						[{text: 'Τόπος Γέννησης', fillColor: '#77caea'}, data[11].innerHTML],
						[{text: 'Ημερομηνία Γέννησης', fillColor: '#77caea'}, data[13].innerHTML],
						[{text: 'Αριθμός Δελτίου Ταυτότητας ή Διαβατηρίου', fillColor: '#77caea'}, data[15].innerHTML],
						[{text: 'Ημερομηνία Έκδοσης Ταυτότητας ή Διαβατηρίου', fillColor: '#77caea'}, data[17].innerHTML],
						[{text: 'Α.Φ.Μ.', fillColor: '#77caea'}, data[19].innerHTML],
						[{text: 'Δ.Ο.Υ.', fillColor: '#77caea'}, data[21].innerHTML],
						[{text: 'Α.Μ.Κ.Α.', fillColor: '#77caea'}, data[23].innerHTML],
						[{text: 'Οδός', fillColor: '#77caea'}, data[25].innerHTML],
						[{text: 'Αριθμός', fillColor: '#77caea'}, data[27].innerHTML],
						[{text: 'Τ.Κ.', fillColor: '#77caea'}, data[29].innerHTML],
						[{text: 'Πόλη', fillColor: '#77caea'}, data[31].innerHTML],
						[{text: 'Νομός', fillColor: '#77caea'}, data[33].innerHTML],
						[{text: 'Τηλέφωνο', fillColor: '#77caea'}, data[35].innerHTML],
						[{text: 'Ηλεκτρονική Διεύθυνση', fillColor: '#77caea'}, data[37].innerHTML],
						[{text: 'Οικογενειακή Κατάσταση', fillColor: '#77caea'}, data[39].innerHTML],
						[{text: 'Τέκνα', fillColor: '#77caea'}, data[41].innerHTML],
						[{text: 'Εργασιακή Σχέση', fillColor: '#77caea'}, data[43].innerHTML],
						[{text: 'Επιχείρηση-Ίδρυμα', fillColor: '#77caea'}, data[45].innerHTML]
					]	
				},
				pageBreak: 'after'
			},
			{text: '\nO/H κάτωθι υπογράφων/ουσα δηλώνω υπεύθυνα ότι τα παραπάνω στοιχεία είναι πλήρη και αληθή και ότι έλαβα γνώση για την Πολιτική Απορρήτου και Διαχείρησης Προσωπικών Δεδομένων της εταιρείας ΧΧΧ, την οποία και αποδέχομαι, Επιπλέον, αποδέχομαι την αποστολή της παρούσας αίτησης, καθώς και οποιοδήποτε αποδεικτικών εγγράφων μου ζητηθούν.\n', alignment: 'justify'},
			{
				table: {
					widths: [ '*', 320],
					body: [
						[{text: 'Υπογραφή', fillColor: '#77caea'}, '\n\n\n'],
						[{text: 'Ημερομηνία Υπογραφής', fillColor: '#77caea'}, '\n\n\n']
					]	
				}
			}
		]
		
	};
	
	pdfMake.createPdf(docDefinition).download('registration_form.pdf');
}