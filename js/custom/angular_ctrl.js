// δημιουργία AngularJS εφαρμογής
var app = angular.module('form_app',[]);

// βασικός ελεγκτής της εφαρμογής
app.controller('MainCtrl', function($scope, $timeout, $http, $location) {
	
	// αρχική προβολή
	$scope.showForm = true;
	$scope.confirmForm = false;
	$scope.completedForm = false;
	
	// αρχικοποίηση δεδομένων
	$scope.formData = {};
	
	// αρχικοποιήση τέκνων (default value)
	$scope.formData.children = 0;
	
	// μέθοδος ελέγχου και μορφοποίησης δεδομένων
	$scope.validationForm = function(){
		
		// έλεγχος αν τα δεδομένα πέρασαν στον validator
		if($('#contact_form').data('bootstrapValidator').isValid()){
			
			// αλλαγή προβολής (στον πίνακα επιβεβαίωσης δεδομένων)
			$scope.showForm = false;
			$scope.confirmForm = true;
			$scope.completedForm = false;
			
			// μορφοποίηση φύλλου σε Α και Θ (περισσότερο ασφαλές από το name='ελληνικές_λέξεις')
			if($scope.formData.gender=='1') $scope.formData.genderName="Α";
			else $scope.formData.genderName="Θ";
			
			// μορφοποίηση οικογενειακής κατάστασης
			if($scope.formData.marital == '1') $scope.formData.maritalStatus = "Έγγαμος";
			else $scope.formData.maritalStatus = "Άγαμος";
			
			// μορφοποίηση εργασιακής σχέσης
			if($scope.formData.labour == '1') $scope.formData.labourStatus = "Ελεύθερος Επαγγελματίας";
			else if($scope.formData.labour == '2') $scope.formData.labourStatus = "Μισθωτός Ιδιωτικού Τομέα";
			else if($scope.formData.labour == '3') $scope.formData.labourStatus = "Μισθωτός Δημοσίου Τομέα";
			else $scope.formData.labourStatus = "Άλλο";
			
		}
		// αν εμφανιστεί σφάλμα που δε χειρίζεται ο validator
		else console.log('failure');
		
		// εμφάνιση στην κονσόλα των τελικών δεδομένων
		console.log($scope.formData);
	};
	
	// μέθοδος εμφάνισης της φόρμας (back)
	$scope.returnToForm = function(){
		$scope.showForm = true;
		$scope.confirmForm = false;
		$scope.completedForm = false;
	};
	
	// μέθοδος μετάβασης στην τρίτη προβολή (αποθήκευση δεδομένων και κατέβασμα pdf)
	$scope.completeForm = function(){
		$scope.showForm = false;
		$scope.confirmForm = false;
		$scope.completedForm = true;
		exportPDF();
		
		// αποθήκευση δεδομένων στη βάση δεδομένων
		$http.post("controllers/backend_insertdata.php", this.formData).then(function(response) {
			var data = response.data;
			console.log(data);
        });
		
	};
	

});

// μορφή datepicker
$(function() {
	$('input[id="datepicker"]').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		locale: {
			  format: 'DD-MM-YYYY'
		}
		}
	)
});
