// εφαρμογή validator κατά την εμφάνιση της φόρμας
$(document).ready(function() {
	$('#contact_form').bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			first_name: {
				validators: {
						stringLength: {
						min: 2,
					},
						notEmpty: {
						message: 'Παρακαλώ συμπληρώστε το όνομά σας'
					}
				}
			},
			last_name: {
				validators: {
					 stringLength: {
						min: 2,
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε το επώνυμό σας'
					}
				}
			},
			father_name: {
				validators: {
					 stringLength: {
						min: 2,
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε το πατρώνυμό σας'
					}
				}
			},
			citizenship: {
				validators: {
					 stringLength: {
						min: 2,
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε την υπηκοότητά σας'
					}
				}
			},
			gender: {
				validators: {
					notEmpty: {
						message: 'Παρακαλώ επιλέξτε τo φύλο σας'
					}
				}
			},
			birthplace: {
				validators: {
					 stringLength: {
						min: 4,
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε τον τόπο γεννήσεώς σας'
					}
				}
			},
			birthdate: {
				validators: {
					date: {
                        format: 'DD-MM-YYYY',
                        message: 'Συμπληρώστε μία έγκυρη ημερομηνία γεννήσεως'
                    },
					/*callback: {
						message: 'Δεν είναι έγκυρη η ημερομηνία',
						callback: function (value, validator, $field) {
							// value is the value of field
							// validator is instance of BootstrapValidator
							// $field is the jQuery object representing the field element
							var d = new Date();
							var yearCompare = (d.getFullYear()) - value.getFullYear();
							
							return yearCompare > 18;
						}
					}*/
				}
			},
			paper_id: {
				validators: {
					 stringLength: {
						min: 2,
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε τον Α.Δ.Τ. ή το Αρ. Διαβ. σας'
					}
				}
			},
			id_date: {
				validators: {
					date: {
                        format: 'DD-MM-YYYY',
                        message: 'Συμπληρώστε μία έγκυρη ημερομηνία γεννήσεως'
                    }
				}
			},
			vat_num: {
				validators: {
					 stringLength: {
						min: 9,
						max: 9,
						message: 'Απαιτούνται 9 ακριβώς ψηφία'
					},
					/*integer: {
						message: 'Απαιτούνται 9 ψηφία',
						callback: function (value, validator, $field) {
							var number = value.toString();
						}
					},*/
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε τον Α.Φ.Μ. σας'
					},
					callback: {
						message: 'Δεν είναι έγκυρος ο Α.Φ.Μ. σας',
						callback: function (value, validator, $field) {
							var afmDigits = [];
							var sAfm = value.toString();
							for (var i = 0; i < sAfm.length; i += 1) afmDigits.push(parseInt(sAfm.charAt(i)));
							var checkAFM = afmDigits[0]*256 + afmDigits[1]*128 + afmDigits[2]*64 + afmDigits[3]*32 + afmDigits[4]*16 + afmDigits[5]*8 + afmDigits[6]*4 + afmDigits[7]*2;
							checkAFM %= 11;
							checkAFM %= 10;
							return checkAFM == afmDigits[8];
						}
					}
				}
			},
			vat_service: {
				validators: {
					notEmpty: {
						message: 'Παρακαλώ επιλέξτε Δ.Ο.Υ.'
					}
				}
			},
			sec_num: {
				validators: {
					 stringLength: {
						min: 11,
						max: 11,
						message: 'Απαιτούνται 11 ακριβώς ψηφία'
					},
					/*integer: {
						message: 'Απαιτούνται 11 ακριβώς ψηφία'
					},*/
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε τον A.M.K.A. σας'
					},
					callback: {
						message: 'Δεν είναι έγκυρος ο A.M.K.A. σας',
						callback: function (value, validator, $field) {
							var amkaDigits = [];
							var sAmka = value.toString();
							for (var i = 0; i < sAmka.length; i += 1) amkaDigits.push(parseInt(sAmka.charAt(i)));
							var checkAMKA = 0;
							for (var i=0; i<amkaDigits.length; i++ ){
								if((i+2)%2==0){
									checkAMKA += amkaDigits[i];
								}
								else {
									var mpl = 2 * amkaDigits[i];
									if(mpl >= 10) {
										checkAMKA += parseInt(mpl/10) + (mpl%10);
									}
									else checkAMKA += mpl;
								}
								
							};
							checkAMKA %= 10;
							return checkAMKA == 0;
						}
					}
				}
			},
			address: {
				validators: {
					 stringLength: {
						min: 3,
						message: 'Παρακαλώ συμπληρώστε τη διεύθυνσή σας'
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε τη διεύθυνσή σας'
					}
				}
			},
			address_num: {
				validators: {
					 stringLength: {
						min: 1,
						message: 'Παρακαλώ συμπληρώστε τον αριθμό διεθύνσεώς σας (1 εάν δεν υπάρχει)'
					},
					integer: {
						message: 'Παρακαλώ συμπληρώστε τον αριθμό διεθύνσεώς σας (1 εάν δεν υπάρχει)'
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε τον αριθμό διεθύνσεώς σας (1 εάν δεν υπάρχει)'
					}
				}
			},
			zip_code: {
				validators: {
					stringLength: {
						min: 1,
						message: 'Παρακαλώ συμπληρώστε Τ.Κ. ή την Τ.Θ. σας'
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε Τ.Κ. ή την Τ.Θ. σας'
					}
				}
			},
			city: {
				validators: {
					 stringLength: {
						min: 4,
						message: 'Παρακαλώ συμπληρώστε την πόλη σας'
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε την πόλη σας'
					}
				}
			},
			province: {
				validators: {
					 stringLength: {
						min: 4,
						message: 'Παρακαλώ συμπληρώστε το νομό σας'
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε το νομό σας'
					}
				}
			},
			phone: {
				validators: {
					stringLength: {
						min: 10,
						max: 10,
						message: 'Απαιτούνται 10 ακριβώς ψηφία'
					},
					integer: {
						message: 'Απαιτούνται 10 ακριβώς ψηφία'
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε ένα τηλέφωνο επικοινωνίας'
					}
				}
			},
			email: {
				validators: {
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε το email σας'
					},
					emailAddress: {
						message: 'Μη έγκυρη μορφή email'
					}
				}
			},
			marital: {
				validators: {
					notEmpty: {
						message: 'Παρακαλώ επιλέξτε οικογενειακή κατάσταση'
					}
				}
			},
			labour: {
				validators: {
					notEmpty: {
						message: 'Παρακαλώ επιλέξτε εργασιακή σχέση'
					}
				}
			},
			labour_name: {
				validators: {
					stringLength: {
						min: 3,
						message: 'Παρακαλώ συμπληρώστε την επιχείρηση/ίδρυμα που εργάζεστε'
					},
					notEmpty: {
						message: 'Παρακαλώ συμπληρώστε την επιχείρηση/ίδρυμα που εργάζεστε'
					}
				}
			}
			
			}
		})
		.on('success.form.bv', function(e) {

			// Prevent form submission
			e.preventDefault();
		
		});
		
		$('#contact_form').bootstrapValidator('validate');
		
});