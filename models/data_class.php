<?php

/******************************************************************************/
/* sqlite pdo class ***********************************************************/
/* author: Michail Maridakis **************************************************/
/* email: michael.marid@gmail.com *********************************************/
/******************************************************************************/

class SQLITE_PDO {

	// μεταβλητή για το pdo
	private $pdo;

	// κατασκευαστής (σύνδεση με ΒΔ)
	public function __construct($db){
		try{
			if($this->pdo == null){
				$this->pdo = new PDO("sqlite:$db", null, null, array(PDO::ATTR_PERSISTENT => true));
			}
			return $this->pdo;
		}catch(PDOException $e){
			echo "Connection failed: " . $e->getMessage();
		}
	}

	// μέθοδος που καλείται πάντα για την εκτέλεση του ερωτήματος
	private function makeStatement($sql, $data = NULL){ // 1 or 2 arguments
		$statement = $this->pdo->prepare($sql);
		try {
			$statement->execute($data);
		}
		catch(Exception $e){ // αδυναμία εκτέλεσης
			$msg = "<p>You tried to run this sql: $sql</p> <p>Exception: $e</p>";
			trigger_error($msg);
		}
		return $statement;
	}

	// δημιουργία της ΒΔ (καλείται μόνο μία φορά)
	public function createDatabase(){
		$sql = "CREATE TABLE IF NOT EXISTS members (id INTEGER PRIMARY KEY AUTOINCREMENT, date_created DATE NOT NULL, first_name TEXT NOT NULL, last_name TEXT NOT NULL, father_name TEXT NOT NULL, citizenship TEXT NOT NULL, gender TEXT NOT NULL, birth_place TEXT NOT NULL, birth_date DATE NOT NULL, paper_id TEXT NOT NULL, id_date DATE NOT NULL, vat_num TEXT NOT NULL, vat_service TEXT NOT NULL, security_num TEXT NOT NULL, street_road TEXT NOT NULL, street_num TEXT NOT NULL, postal TEXT NOT NULL, city TEXT NOT NULL, province TEXT NOT NULL, telephone TEXT NOT NULL, email TEXT NOT NULL, marital_status TEXT NOT NULL, children INTEGER NOT NULL, labour_status TEXT NOT NULL, labour_name TEXT NOT NULL)";
		$statement = $this->makeStatement($sql);
	}

	// αποθήκευση στη ΒΔ
	public function save($firstName, $lastName, $fatherName, $citizenShip, $genderName, $birthPlace, $birthDate, $paperId, $idDate, $vatNum, $vatService, $securityNum, $streetRoad, $streetNum, $postal, $city, $province, $telephone, $email, $maritalStatus, $children, $labourStatus, $labourName){
		$sql = "INSERT INTO members (date_created, first_name, last_name, father_name, citizenship, gender, birth_place, birth_date, paper_id, id_date, vat_num, vat_service, security_num, street_road, street_num, postal, city, province, telephone, email, marital_status, children, labour_status, labour_name) VALUES (CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
		$data = array($firstName, $lastName, $fatherName, $citizenShip, $genderName, $birthPlace, $birthDate, $paperId, $idDate, $vatNum, $vatService, $securityNum, $streetRoad, $streetNum, $postal, $city, $province, $telephone, $email, $maritalStatus, $children, $labourStatus, $labourName);
		$statement = $this->makeStatement($sql, $data);
	}










	// μέθοδος για encryption σε string (δεν έχει εφαρμογή εδώ)
	private function simpleEncryption($string, $action = 'e' ) {
		$secret_key = 'prudential_it_rules';
		$secret_iv = 'wtf_is_the_iv';

		$output = false;
		$encrypt_method = "AES-256-CBC";
		$key = hash('sha256', $secret_key);
		$iv = substr(hash('sha256', $secret_iv), 0, 16);

		if( $action == 'e' ) {
			$output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
		}
		else if( $action == 'd' ){
			$output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
		}
		return $output;
	}




}

?>
