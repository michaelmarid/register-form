# register-form

This is a modern registration form, using PHP, AngularJS, PDFMakeJS, DaterangePickerJS, BootstrapValidatorJS.
The example form is in greek language and it contains custom validators like greek VAT and security number.