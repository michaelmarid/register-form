<?php

return "

<div id='formAJS' ng-app='form_app' ng-controller='MainCtrl'>

	<!-- ************************************** ΠΡΟΒΟΛΗ 1 - ΦΟΡΜΑ ΠΡΟΣ ΣΥΜΠΛΗΡΩΣΗ ************************************** -->

	<div ng-show='showForm'>
	 
		<form class='well form-horizontal' action='' method='post'  id='contact_form'>
			
			<fieldset>
				<legend>Φόρμα Εγγραφής</legend>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Όνομα</label>
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-user'></i></span>
							<input name='first_name' placeholder='Όνομα' class='form-control' type='text' ng-model='formData.firstName'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Επώνυμο</label> 
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-user'></i></span>
							<input name='last_name' placeholder='Επώνυμο' class='form-control' type='text' ng-model='formData.lastName'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Πατρώνυμο</label> 
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-user'></i></span>
							<input name='father_name' placeholder='Πατρώνυμο' class='form-control' type='text' ng-model='formData.fatherName'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Υπηκοότητα</label> 
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-user'></i></span>
							<input name='citizenship' placeholder='Υπηκοότητα' class='form-control' type='text' ng-model='formData.citizenShip'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Φύλο</label>
					<div class='col-md-6'>
						<div class='radio'>
							<label>
								<input type='radio' ng-model='formData.gender' name='gender' value='1' /> Α
							</label>
						</div>
						<div class='radio'>
							<label>
								<input type='radio'  ng-model='formData.gender' name='gender' value='2' /> Θ
							</label>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Τόπος Γεννήσεως</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-home'></i></span>
							<input name='birthplace' placeholder='Τόπος Γεννήσεως' class='form-control' type='text' ng-model='formData.birthPlace'>
						</div>
					</div>
				</div>
				
				<div class='form-group'> 
					<label class='col-md-4 control-label'>Ημερομηνία Γεννήσεως</label>
					<div class='col-md-6 selectContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-calendar'></i></span>
							<input type='text' id='datepicker' name='birthdate' class='form-control' ng-model='formData.birthDate' />
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Αριθμός Δελτίου Ταυτότητας ή Διαβατηρίου</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-credit-card'></i></span>
							<input name='paper_id' placeholder='ΑΔΤ/ΔΙΑΒ' class='form-control' type='text' ng-model='formData.paperId'>
						</div>
					</div>
				</div>
				
				<div class='form-group'> 
					<label class='col-md-4 control-label'>Ημερομηνία Έκδοσης Ταυτότητας ή Διαβατηρίου</label>
					<div class='col-md-6 selectContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-calendar'></i></span>
							<input type='text' id='datepicker' name='id_date' class='form-control' ng-model='formData.idDate' />
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Α.Φ.Μ.</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-credit-card'></i></span>
							<input name='vat_num' placeholder='Α.Φ.Μ.' class='form-control' type='text' ng-model='formData.vatNum'>
						</div>
					</div>
				</div>
				
				<div class='form-group'> 
					<label class='col-md-4 control-label'>Δ.Ο.Υ.</label>
					<div class='col-md-6 selectContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-credit-card'></i></span>
							<select ng-model='formData.vatService' name='vat_service' class='form-control selectpicker' >
								<option value='' >Δ.Ο.Υ.</option>
								<option value='1129-ΑΓΙΟΥ ΔΗΜΗΤΡΙΟΥ'>ΑΓΙΟΥ ΔΗΜΗΤΡΙΟΥ</option>
								<option value='8221-ΑΓΙΟΥ ΝΙΚΟΛΑΟΥ'>ΑΓΙΟΥ ΝΙΚΟΛΑΟΥ</option>
								<option value='1136-ΑΓΙΩΝ ΑΝΑΡΓΥΡΩΝ'>ΑΓΙΩΝ ΑΝΑΡΓΥΡΩΝ</option>
								<option value='1552-ΑΓΡΙΝΙΟΥ'>ΑΓΡΙΝΙΟΥ</option>
								<option value='1137-ΑΙΓΑΛΕΩ'>ΑΙΓΑΛΕΩ</option>
								<option value='2311-ΑΙΓΙΟΥ'>ΑΙΓΙΟΥ</option>
								<option value='1101-Α΄ ΑΘΗΝΩΝ' selected='selected'>Α΄ ΑΘΗΝΩΝ</option>
								<option value='1104-Δ΄ ΑΘΗΝΩΝ'>Δ΄ ΑΘΗΝΩΝ</option>
								<option value='1112-ΙΒ΄ ΑΘΗΝΩΝ'>ΙΒ΄ ΑΘΗΝΩΝ</option>
								<option value='1113-ΙΓ΄ ΑΘΗΝΩΝ'>ΙΓ΄ ΑΘΗΝΩΝ</option>
								<option value='1114-ΙΔ΄ ΑΘΗΝΩΝ'>ΙΔ΄ ΑΘΗΝΩΝ</option>
								<option value='1117-ΙΖ ΑΘΗΝΩΝ'>ΙΖ ΑΘΗΝΩΝ</option>
								<option value='1106-ΣΤ΄ ΑΘΗΝΩΝ'>ΣΤ΄ ΑΘΗΝΩΝ</option>
								<option value='5211-ΑΛΕΞΑΝΔΡΟΥΠΟΛΗΣ'>ΑΛΕΞΑΝΔΡΟΥΠΟΛΗΣ</option>
								<option value='2411-ΑΜΑΛΙΑΔΑΣ'>ΑΜΑΛΙΑΔΑΣ</option>
								<option value='1135-ΑΜΑΡΟΥΣΙΟΥ'>ΑΜΑΡΟΥΣΙΟΥ</option>
								<option value='4233-ΑΜΠΕΛΟΚΗΠΩΝ'>ΑΜΠΕΛΟΚΗΠΩΝ</option>
								<option value='1912-ΑΜΦΙΣΣΑΣ'>ΑΜΦΙΣΣΑΣ</option>
								<option value='9311-ΑΡΓΟΣΤΟΛΙΟΥ'>ΑΡΓΟΣΤΟΛΙΟΥ</option>
								<option value='2111-ΑΡΓΟΥΣ'>ΑΡΓΟΥΣ</option>
								<option value='6111-ΑΡΤΑΣ'>ΑΡΤΑΣ</option>
								<option value='1302-ΑΧΑΡΝΩΝ'>ΑΧΑΡΝΩΝ</option>
								<option value='4112-ΒΕΡΟΙΑΣ'>ΒΕΡΟΙΑΣ</option>
								<option value='3321-ΒΟΛΟΥ'>ΒΟΛΟΥ</option>
								<option value='1152-ΒΥΡΩΝΟΣ'>ΒΥΡΩΝΟΣ</option>
								<option value='1179-ΓΑΛΑΤΣΙΟΥ'>ΓΑΛΑΤΣΙΟΥ</option>
								<option value='4621-ΓΙΑΝΝΙΤΣΩΝ'>ΓΙΑΝΝΙΤΣΩΝ</option>
								<option value='1139-ΓΛΥΦΑΔΑΣ'>ΓΛΥΦΑΔΑΣ</option>
								<option value='4521-ΓΡΕΒΕΝΩΝ'>ΓΡΕΒΕΝΩΝ</option>
								<option value='5111-ΔΡΑΜΑΣ'>ΔΡΑΜΑΣ</option>
								<option value='4631-ΕΔΕΣΣΑΣ'>ΕΔΕΣΣΑΣ</option>
								<option value='1303-ΕΛΕΥΣΙΝΑΣ'>ΕΛΕΥΣΙΝΑΣ</option>
								<option value='9111-ΖΑΚΥΝΘΟΥ'>ΖΑΚΥΝΘΟΥ</option>
								<option value='6211-ΗΓΟΥΜΕΝΙΤΣΑΣ'>ΗΓΟΥΜΕΝΙΤΣΑΣ</option>
								<option value='1173-ΗΛΙΟΥΠΟΛΗΣ'>ΗΛΙΟΥΠΟΛΗΣ</option>
								<option value='8110-ΗΡΑΚΛΕΙΟΥ'>ΗΡΑΚΛΕΙΟΥ</option>
								<option value='4211-Α΄ ΘΕΣΣΑΛΟΝΙΚΗΣ'>Α΄ ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='4214-Δ΄ ΘΕΣΣΑΛΟΝΙΚΗΣ'>Δ΄ ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='4215-Ε΄ ΘΕΣΣΑΛΟΝΙΚΗΣ'>Ε΄ ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='4216-ΣΤ΄ ΘΕΣΣΑΛΟΝΙΚΗΣ'>ΣΤ΄ ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='4217-Ζ΄ ΘΕΣΣΑΛΟΝΙΚΗΣ'>Ζ΄ ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='4228-Η΄ ΘΕΣΣΑΛΟΝΙΚΗΣ'>Η΄ ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='1411-ΘΗΒΩΝ'>ΘΗΒΩΝ</option>
								<option value='7121-ΘΗΡΑΣ'>ΘΗΡΑΣ</option>
								<option value='6311-ΙΩΑΝΝΙΝΩΝ'>ΙΩΑΝΝΙΝΩΝ</option>
								<option value='4234-ΙΩΝΙΑΣ ΘΕΣΣΑΛΟΝΙΚΗΣ'>ΙΩΝΙΑΣ ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='5321-ΚΑΒΑΛΑΣ'>ΚΑΒΑΛΑΣ</option>
								<option value='4232-ΚΑΛΑΜΑΡΙΑΣ'>ΚΑΛΑΜΑΡΙΑΣ</option>
								<option value='2711-ΚΑΛΑΜΑΤΑΣ'>ΚΑΛΑΜΑΤΑΣ</option>
								<option value='1130-ΚΑΛΛΙΘΕΑΣ'>ΚΑΛΛΙΘΕΑΣ</option>
								<option value='3111-ΚΑΡΔΙΤΣΑΣ'>ΚΑΡΔΙΤΣΑΣ</option>
								<option value='1611-ΚΑΡΠΕΝΗΣΙΟΥ'>ΚΑΡΠΕΝΗΣΙΟΥ</option>
								<option value='4311-ΚΑΣΤΟΡΙΑΣ'>ΚΑΣΤΟΡΙΑΣ</option>
								<option value='4711-ΚΑΤΕΡΙΝΗΣ'>ΚΑΤΕΡΙΝΗΣ</option>
								<option value='1125-ΚΑΤΟΙΚΩΝ  ΕΞΩΤΕΡΙΚΟΥ'>ΚΑΤΟΙΚΩΝ  ΕΞΩΤΕΡΙΚΟΥ</option>
								<option value='1118-ΚΕΝΤΡΟ ΕΛΕΓΧΟΥ ΜΕΓΑΛΩΝ ΕΠΙΧΕΙΡΗΣΕΩΝ'>ΚΕΝΤΡΟ ΕΛΕΓΧΟΥ ΜΕΓΑΛΩΝ ΕΠΙΧΕΙΡΗΣΕΩΝ</option>
								<option value='9211-ΚΕΡΚΥΡΑΣ'>ΚΕΡΚΥΡΑΣ</option>
								<option value='1153-ΚΗΦΙΣΙΑΣ'>ΚΗΦΙΣΙΑΣ</option>
								<option value='4411-ΚΙΛΚΙΣ'>ΚΙΛΚΙΣ</option>
								<option value='4541-ΚΟΖΑΝΗΣ'>ΚΟΖΑΝΗΣ</option>
								<option value='5511-ΚΟΜΟΤΗΝΗΣ'>ΚΟΜΟΤΗΝΗΣ</option>
								<option value='2513-ΚΟΡΙΝΘΟΥ'>ΚΟΡΙΝΘΟΥ</option>
								<option value='1304-ΚΟΡΩΠΙΟΥ'>ΚΟΡΩΠΙΟΥ</option>
								<option value='1722-ΚΥΜΗΣ'>ΚΥΜΗΣ</option>
								<option value='7531-ΚΩ'>ΚΩ</option>
								<option value='4222-ΛΑΓΚΑΔΑ'>ΛΑΓΚΑΔΑ</option>
								<option value='1832-ΛΑΜΙΑΣ'>ΛΑΜΙΑΣ</option>
								<option value='3231-Α΄ ΛΑΡΙΣΑΣ'>Α΄ ΛΑΡΙΣΑΣ</option>
								<option value='3232-Β΄ ΛΑΡΙΣΑΣ'>Β΄ ΛΑΡΙΣΑΣ</option>
								<option value='9421-ΛΕΥΚΑΔΑΣ'>ΛΕΥΚΑΔΑΣ</option>
								<option value='1421-ΛΙΒΑΔΕΙΑΣ'>ΛΙΒΑΔΕΙΑΣ</option>
								<option value='1531-ΜΕΣΟΛΟΓΓΙΟΥ'>ΜΕΣΟΛΟΓΓΙΟΥ</option>
								<option value='1211-ΜΟΣΧΑΤΟΥ'>ΜΟΣΧΑΤΟΥ</option>
								<option value='7172-ΜΥΚΟΝΟΥ'>ΜΥΚΟΝΟΥ</option>
								<option value='7231-ΜΥΤΙΛΗΝΗΣ'>ΜΥΤΙΛΗΝΗΣ</option>
								<option value='7151-ΝΑΞΟΥ'>ΝΑΞΟΥ</option>
								<option value='2131-ΝΑΥΠΛΙΟΥ'>ΝΑΥΠΛΙΟΥ</option>
								<option value='1131-ΝΕΑΣ ΙΩΝΙΑΣ'>ΝΕΑΣ ΙΩΝΙΑΣ</option>
								<option value='3323-ΝΕΑΣ ΙΩΝΙΑΣ ΒΟΛΟΥ'>ΝΕΑΣ ΙΩΝΙΑΣ ΒΟΛΟΥ</option>
								<option value='1132-ΝΕΑΣ ΣΜΥΡΝΗΣ'>ΝΕΑΣ ΣΜΥΡΝΗΣ</option>
								<option value='1145-ΝΕΟΥ ΗΡΑΚΛΕΙΟΥ'>ΝΕΟΥ ΗΡΑΚΛΕΙΟΥ</option>
								<option value='4923-ΝΕΩΝ ΜΟΥΔΑΝΙΩΝ'>ΝΕΩΝ ΜΟΥΔΑΝΙΩΝ</option>
								<option value='1220-ΝΙΚΑΙΑΣ'>ΝΙΚΑΙΑΣ</option>
								<option value='5411-ΞΑΝΘΗΣ'>ΞΑΝΘΗΣ</option>
								<option value='5231-ΟΡΕΣΤΙΑΔΑΣ'>ΟΡΕΣΤΙΑΔΑΣ</option>
								<option value='1133-ΠΑΛΑΙΟΥ ΦΑΛΗΡΟΥ'>ΠΑΛΑΙΟΥ ΦΑΛΗΡΟΥ</option>
								<option value='1312-ΠΑΛΛΗΝΗΣ'>ΠΑΛΛΗΝΗΣ</option>
								<option value='7161-ΠΑΡΟΥ'>ΠΑΡΟΥ</option>
								<option value='2331-Α΄ ΠΑΤΡΩΝ'>Α΄ ΠΑΤΡΩΝ</option>
								<option value='2334-Γ΄ ΠΑΤΡΩΝ'>Γ΄ ΠΑΤΡΩΝ</option>
								<option value='1201-Α΄ ΠΕΙΡΑΙΑ'>Α΄ ΠΕΙΡΑΙΑ</option>
								<option value='1203-Γ΄ ΠΕΙΡΑΙΑ'>Γ΄ ΠΕΙΡΑΙΑ</option>
								<option value='1204-Δ΄ ΠΕΙΡΑΙΑ'>Δ΄ ΠΕΙΡΑΙΑ</option>
								<option value='1205-Ε΄ ΠΕΙΡΑΙΑ'>Ε΄ ΠΕΙΡΑΙΑ</option>
								<option value='1138-Α΄ ΠΕΡΙΣΤΕΡΙΟΥ'>Α΄ ΠΕΡΙΣΤΕΡΙΟΥ</option>
								<option value='1157-Β΄ ΠΕΡΙΣΤΕΡΙΟΥ'>Β΄ ΠΕΡΙΣΤΕΡΙΟΥ</option>
								<option value='1207-ΠΛΟΙΩΝ ΠΕΙΡΑΙΑ'>ΠΛΟΙΩΝ ΠΕΙΡΑΙΑ</option>
								<option value='2231-ΤΡΙΠΟΛΗΣ'>ΤΡΙΠΟΛΗΣ</option>
								<option value='1120-ΥΠΗΡΕΣΙΑ ΕΣΩΤΕΡΙΚΗΣ ΕΠΑΝΕΞΕΤΑΣΗΣ'>ΥΠΗΡΕΣΙΑ ΕΣΩΤΕΡΙΚΗΣ ΕΠΑΝΕΞΕΤΑΣΗΣ</option>
								<option value='4922-ΠΟΛΥΓΥΡΟΥ'>ΠΟΛΥΓΥΡΟΥ</option>
								<option value='6411-ΠΡΕΒΕΖΑΣ'>ΠΡΕΒΕΖΑΣ</option>
								<option value='4531-ΠΤΟΛΕΜΑΙΔΑΣ'>ΠΤΟΛΕΜΑΙΔΑΣ</option>
								<option value='2412-ΠΥΡΓΟΥ'>ΠΥΡΓΟΥ</option>
								<option value='8341-ΡΕΘΥΜΝΟΥ'>ΡΕΘΥΜΝΟΥ</option>
								<option value='7542-ΡΟΔΟΥ'>ΡΟΔΟΥ</option>
								<option value='7322-ΣΑΜΟΥ'>ΣΑΜΟΥ</option>
								<option value='5621-ΣΕΡΡΩΝ'>ΣΕΡΡΩΝ</option>
								<option value='2632-ΣΠΑΡΤΗΣ'>ΣΠΑΡΤΗΣ</option>
								<option value='7171-ΣΥΡΟΥ'>ΣΥΡΟΥ</option>
								<option value='3412-ΤΡΙΚΑΛΩΝ'>ΤΡΙΚΑΛΩΝ</option>
								<option value='1159-Φ.Α.Ε. ΑΘΗΝΩΝ'>Φ.Α.Ε. ΑΘΗΝΩΝ</option>
								<option value='4224-Φ.Α.Ε. ΘΕΣΣΑΛΟΝΙΚΗΣ'>Φ.Α.Ε. ΘΕΣΣΑΛΟΝΙΚΗΣ</option>
								<option value='1206-Φ.Α.Ε. ΠΕΙΡΑΙΑ'>Φ.Α.Ε. ΠΕΙΡΑΙΑ</option>
								<option value='4812-ΦΛΩΡΙΝΑΣ'>ΦΛΩΡΙΝΑΣ</option>
								<option value='1134-ΧΑΛΑΝΔΡΙΟΥ'>ΧΑΛΑΝΔΡΙΟΥ</option>
								<option value='1732-ΧΑΛΚΙΔΑΣ'>ΧΑΛΚΙΔΑΣ</option>
								<option value='8431-ΧΑΝΙΩΝ'>ΧΑΝΙΩΝ</option>
								<option value='7411-ΧΙΟΥ'>ΧΙΟΥ</option>
								<option value='1151-ΧΟΛΑΡΓΟΥ'>ΧΟΛΑΡΓΟΥ</option>
								<option value='1175-ΨΥΧΙΚΟΥ'>ΨΥΧΙΚΟΥ</option>
							</select>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Α.Μ.Κ.Α.</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-credit-card'></i></span>
							<input name='sec_num' placeholder='Α.Μ.Κ.Α.' class='form-control' type='text' ng-model='formData.secNum'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Οδός</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-home'></i></span>
							<input name='address' placeholder='Οδός' class='form-control' type='text' ng-model='formData.address'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Αριθμός</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-home'></i></span>
							<input name='address_num' placeholder='Αριθμός' class='form-control' type='text' ng-model='formData.addressNum'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Τ.Κ. ή Τ.Θ.</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-home'></i></span>
							<input name='zip_code' placeholder='Τ.Κ. ή Τ.Θ.' class='form-control' type='text' ng-model='formData.zipCode'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Πόλη</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-home'></i></span>
							<input name='city' placeholder='Πόλη' class='form-control' type='text' ng-model='formData.city'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Νομός</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-home'></i></span>
							<input name='province' placeholder='Νομός' class='form-control' type='text' ng-model='formData.province'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Τηλέφωνο</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-earphone'></i></span>
							<input name='phone' placeholder='21012345678' class='form-control' type='text' ng-model='formData.phone'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>E-Mail</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-envelope'></i></span>
							<input name='email' placeholder='someone@mail.com' class='form-control'  type='text' ng-model='formData.email'>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Οικογενειακή Κατάσταση</label>
					<div class='col-md-6'>
						<div class='radio'>
							<label>
								<input type='radio' ng-model='formData.marital' name='marital' value='0'  /> Άγαμος
							</label>
						</div>
						<div class='radio'>
							<label>
								<input type='radio'  ng-model='formData.marital' name='marital' value='1' /> Έγγαμος
							</label>
						</div>
					</div>
				</div>
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Τέκνα</label>
					<div class='col-md-6'>
						<span style='float:left;'>0</span><span style='float:right;'>5+</span>
						<span style='clear:both'></span>
						<input type='range' name='children' min='0' max='5.01' value='0' step='1' ng-model='formData.children' class='slider' />
						Επιλεγμένη τιμή: {{formData.children}}
					</div>
				</div>

			<div class='form-group'>
					<label class='col-md-4 control-label'>Εργασιακή Σχέση</label>
					<div class='col-md-6'>
						<div class='radio'>
							<label>
								<input type='radio' ng-model='formData.labour' name='labour' value='1' /> Ελεύθερος Επαγγελματίας
							</label>
						</div>
						<div class='radio'>
							<label>
								<input type='radio'  ng-model='formData.labour' name='labour' value='2' /> Μισθωτός Ιδιωτικού Τομέα
							</label>
						</div>
						<div class='radio'>
							<label>
								<input type='radio'  ng-model='formData.labour' name='labour' value='3' /> Μισθωτός Δημοσίου Τομέα
							</label>
						</div>
						<div class='radio'>
							<label>
								<input type='radio'  ng-model='formData.labour' name='labour' value='4' /> Άλλο
							</label>
						</div>
					</div>
				</div>		
				
				<div class='form-group'>
					<label class='col-md-4 control-label'>Όνομα επιχείρησης/ιδρύματος που εργάζεστε</label>  
					<div class='col-md-6 inputGroupContainer'>
						<div class='input-group'>
							<span class='input-group-addon'><i class='glyphicon glyphicon-briefcase'></i></span>
							<input name='labour_name' placeholder='Όνομα Επιχείρησης/Ιδρύματος Εργασίας' class='form-control' type='text' ng-model='formData.labourName'>
						</div>
					</div>
				</div>
				

				<!-- Button -->
				<div class='form-group'>
					<label class='col-md-4 control-label'></label>
						<div class='col-md-6'>
							<button name='submitButton' ng-click='validationForm()' class='btn btn-warning'>Προβολή <span class='glyphicon glyphicon-send'></span></button>
						</div>
				</div>
			
			
			</fieldset>
				
		</form>
	 
	</div> <!-- <div ng-show='showForm'> -->
	
	<!-- ************************************** ΠΡΟΒΟΛΗ 2 - ΠΙΝΑΚΑΣ ΕΠΙΒΕΒΑΙΩΣΗΣ ΦΟΡΜΑΣ ************************************** -->
	
	
	<div ng-show='confirmForm'>
	
		<h3 style='color: white; text-align: center;'>Παρακαλώ επιβεβαιώστε την ορθότητα των στοιχείων ή επιστρέψτε πίσω στη φόρμα.</h3>
		
		<div class='table-responsive' style='background-color: white; color: black;text-align: left;'>
			<table class='table table-bordered' id='toPdf'>
				<thead style='background-color: purple; color: white;'>
					<tr>
						<th>Πεδίο</th>
						<th>Εισαγωγή από Ασφαλισμένο</th>
					</tr>
				</thead>
					<tbody>
						<tr>
							<td>Όνομα</td>
							<td>{{formData.firstName}}</td>
						</tr>
						<tr>
							<td>Επώνυμο</td>
							<td>{{formData.lastName}}</td>
						</tr>
						<tr>
							<td>Πατρώνυμο</td>
							<td>{{formData.fatherName}}</td>
						</tr>
						<tr>
							<td>Υπηκοότητα</td>
							<td>{{formData.citizenShip}}</td>
						</tr>
						<tr>
							<td>Φύλο</td>
							<td>{{formData.genderName}}</td>
						</tr>
						<tr>
							<td>Τόπος Γεννήσεως</td>
							<td>{{formData.birthPlace}}</td>
						</tr>
						<tr>
							<td>Ημερομηνία Γεννήσεως</td>
							<td>{{formData.birthDate}}</td>
						</tr>
						<tr>
							<td>Αριθμός Δελτίου Ταυτότητας ή Διαβατηρίου</td>
							<td>{{formData.paperId}}</td>
						</tr>
						<tr>
							<td>Ημερομηνία Έκδοσης Ταυτότητας ή Διαβατηρίου</td>
							<td>{{formData.idDate}}</td>
							
						</tr>
						<tr>
							<td>Α.Φ.Μ.</td>
							<td>{{formData.vatNum}}</td>
							
						</tr>
						<tr>
							<td>Δ.Ο.Υ.</td>
							<td>{{formData.vatService}}</td>
							
						</tr>
						<tr>
							<td>Α.Μ.Κ.Α.</td>
							<td>{{formData.secNum}}</td>
						</tr>
						<tr>
							<td>Οδός</td>
							<td>{{formData.address}}</td>
						</tr>
						<tr>
							<td>Αριθμός</td>
							<td>{{formData.addressNum}}</td>
						</tr>
						<tr>
							<td>Τ.Κ. ή Τ.Θ.</td>
							<td>{{formData.zipCode}}</td>
						</tr>
						<tr>
							<td>Πόλη</td>
							<td>{{formData.city}}</td>
						</tr>
						<tr>
							<td>Νομός</td>
							<td>{{formData.province}}</td>
						</tr>
						<tr>
							<td>Τηλέφωνο</td>
							<td>{{formData.phone}}</td>
						</tr>
						<tr>
							<td>E-Mail</td>
							<td>{{formData.email}}</td>
						</tr>
						<tr>
							<td>Οικογενειακή Κατάσταση</td>
							<td>{{formData.maritalStatus}}</td>
						</tr>
						<tr>
							<td>Τέκνα</td>
							<td>{{formData.children}}</td>
						</tr>
						<tr>
							<td>Εργασιακή Σχέση</td>
							<td>{{formData.labourStatus}}</td>
						</tr>
						<tr>
							<td>Όνομα επιχείρησης/ιδρύματος εργασίας</td>
							<td>{{formData.labourName}}</td>
						</tr>
				</tbody>
		</table>
		</div>
		
		<div style='display: flex; align-items: center; justify-content: center; margin-top: 20px; margin-bottom: 30px;'>
			<button name='submitButton' ng-click='returnToForm()' class='btn btn-warning'><span class='glyphicon glyphicon-circle-arrow-left'></span> Επιστροφή στη φόρμα </button>
			&nbsp;&nbsp;
			<button ng-click='completeForm()' class='btn btn-danger'><span class='glyphicon glyphicon-ok'></span> Aποθήκευση και παραγωγή αιτήσεως</button>
		</div>
		
	</div>
	
	<!-- ************************************** ΠΡΟΒΟΛΗ 3 - ΑΠΟΘΗΚΕΥΣΗ ΚΑΙ ΕΚΤΥΠΩΣΗ ΦΟΡΜΑΣ ************************************** -->
	
	<div ng-show='completedForm' style='color: white;'>
	
		<h3 style='text-align:center;'>Τα στοιχεία σας έχουν καταχωρηθεί επιτυχώς.</h3>
		<h3 style='text-align:center;'>Κατεβάστε την αίτησή σας:</h3>
		<br/>
		<div style='text-align:center;'>
			<button style='text-align:center;' type='button' class='btn btn-info btn-lg' onClick='exportPDF()'>ΣΥΜΠΛΗΡΩΜΕΝΗ ΑΙΤΗΣΗ ΠΡΟΣ ΥΠΟΓΡΑΦΗ</button>
		</div>
		<h3 style='text-align:center;'>Σας ευχαριστούμε!</h3>
		
	</div>

</div> <!-- <div ng-app='form_app' ng-controller='MainCtrl'> -->

";

?>
